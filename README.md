# WikiSuite Packages

Script and packages for an optimal setup for WikiSuite components, especially [Virtualmin](https://www.virtualmin.com/), [Tiki Manager](https://gitlab.com/tikiwiki/tiki-manager) and [Tiki Wiki CMS Groupware](https://gitlab.com/tikiwiki/tiki).

This reuses the standard [Virtualmin installation script](https://github.com/virtualmin/virtualmin-install/blob/master/virtualmin-install.sh) and will make opiniated choices that make sense for running Tiki instances. For example, it will install Tiki dependencies and multiple versions of PHP. Differences are explained [here](https://wikisuite.org/Differences-from-the-original-script). After running the script, you can configure Webmin/Virtualmin as usual. 


# Requirements
- A special thank you to [Gitlab-buildpkg-tools](https://gitlab.com/Orange-OpenSource/gitlab-buildpkg-tools), which permits to manage packages for several Linux distros using [GitLab CI](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/).
- WikiSuite relies on [Virtualmin, which supports several Operating Systems](https://www.virtualmin.com/os-support/). Please see [historical context](https://wikisuite.org/blogpost16-WikiSuite-will-now-support-all-major-Linux-distros).

And thus, WikiSuite supports a subset of operating systems both supported by Gitlab-buildpkg-tools and Virtualmin.

## Supported
- Debian 10: Reference implementation

## Working but unsupported
- Official support for Ubuntu 20.04 will start with [Virtualmin 7 (In Beta as of 2022-02)](https://github.com/virtualmin/virtualmin-install/commits/master/virtualmin-install.sh). Reason: Virtualmin 6 + Ubuntu 20.04 LTS uses MySQL 8.0 and [Virtualmin 7 will move to MariaDB for all distros](https://github.com/virtualmin/virtualmin-install/issues/42), so we want to avoid a migration.
- Ubuntu 18.04 LTS: Works but given WikiSuite requires a new server, why would anyone want to use this (as of 2022-01)? (and as above, Virtualmin 6 + Ubuntu offers MySQL)

## Planned
- CentOS 7: [End of Life is June 30, 2024](https://endoflife.software/operating-systems/linux/centos)
- Debian 11: when [Virtualmin 7 is released (In Beta as of 2022-02)](https://github.com/virtualmin/virtualmin-install/commits/master/virtualmin-install.sh)
- AlmaLinux 8: when [Virtualmin 7 is released (In Beta as of 2022-02)](https://github.com/virtualmin/virtualmin-install/commits/master/virtualmin-install.sh)
- Rocky Linux 8: when [Virtualmin 7 is released (In Beta as of 2022-02)](https://github.com/virtualmin/virtualmin-install/commits/master/virtualmin-install.sh)
- Ubuntu 22.04 LTS when support is added to Virtualmin (Date unknown)

## Unknown
- CentOS Stream: [Depends on the Virtualmin project. Please see forums for latest discussions](https://forum.virtualmin.com/search?expanded=true&q=centos%20stream%20order%3Alatest)

## Not Supported
- CentOS 8: [Already End of Life](https://endoflife.software/operating-systems/linux/centos)
- Debian 9: [Not supported as will soon be EoL](https://endoflife.software/operating-systems/linux/debian)

# How to use
Please see https://wikisuite.org/How-to-install-WikiSuite#Getting_started
