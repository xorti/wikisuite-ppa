#!/bin/bash

PHP_VERSIONS="5.6 7.1 7.2 7.3 7.4 8.0 8.1"

# cleanup files
for FILE in debian/control debian/postinst debian/*.postinst debian/prerm debian/*.prerm
do
  if [ -f "${FILE}" ]
  then
    echo "Removing ${FILE}"
    rm "${FILE}"
  fi
done

# generate debian files
echo "Adding source package to control"
cat partials/control.part.source > debian/control

for VERSION in ${PHP_VERSIONS}
do
  echo "Adding binary package to control for PHP ${VERSION}"
  cat partials/control.part.binary | sed "s/%VERSION%/${VERSION}/g" >> debian/control
  echo "Adding install file list for PHP ${VERSION}"
  cat partials/install | sed "s/%VERSION%/${VERSION}/g" > debian/wikisuite-php${VERSION}.install
  echo "Adding postinst script for PHP ${VERSION}"
  cat partials/postinst | sed "s/%VERSION%/${VERSION}/g" > debian/wikisuite-php${VERSION}.postinst
  chmod 755 debian/wikisuite-php${VERSION}.postinst
  echo "Adding prerm script for PHP ${VERSION}"
  cat partials/prerm | sed "s/%VERSION%/${VERSION}/g" > debian/wikisuite-php${VERSION}.prerm
  chmod 755 debian/wikisuite-php${VERSION}.prerm
done
